__author__ = 'Abhijeet'

import os

source = ""
sink = ""
fail = ""

def init_crawl(name, base_url):
    #make directory for website
    if not os.path.exists(name):
        print 'creating a directory for website ',name
        os.makedirs(name)

    global source
    source = name +"/source.txt"
    if not os.path.isfile(source):
        print 'creating source file'
        f = open(source,'w')
        f.write(base_url)
        f.close()

    global sink
    sink = name +"/sink.txt"
    if not os.path.isfile(sink):
        print 'creating sink file'
        f = open(sink,'w')
        #f.write("")
        f.close()

    global fail
    fail = name +"/fail.txt"
    if not os.path.isfile(fail):
        print 'creating fail file'
        f = open(fail,'w')
        #f.write("")
        f.close()

def tape_in(filename):
    read = set()
    with open(filename) as f:
        for line in f:
            read.add(line.replace('\n',''))
    return read

def tape_out(weblinks,filename,mode):
    #print 'overwriting file ',filename
    f = open(filename,mode)
    for w in weblinks:
        f.write(w + "\n")
    f.close()

