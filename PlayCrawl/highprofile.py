__author__ = 'Abhijeet'
import os,sys
import redis
from categorize import classify

dir_name = "Crawl01"


print 'Checking direcgory ',dir_name
if not os.path.exists(dir_name):
    print 'directory does not exist ',dir_name
    sys.exit(0)


def printtofile(str):
    #print str
    fresult.write(str)


def parse(fname):
    stars = 5
    author = 'someauthor'
    printflag = False
    with open(fname) as fp:
        stop = False
        flag = False
        count = 3
        for line in fp:
            splt = line.split("==>data<==")

            if 'numDownloads' in splt[0]:

                if "5,000,000 - 10,000,000" in splt[1] or "10,000,000 - 50,000,000" in splt[1] or "50,000,000 - 100,000,000" in splt[1] or "100,000,000 - 500,000,000" in splt[1] or "500,000,000 - 1,000,000,000" in splt[1] or "1,000,000,000 - 5,000,000,000" in splt[1]:
                    printflag = True
                    printtofile("\n===================\n"+splt[1]+"\n")
                    #print splt[1]

            if 'Author' in splt[0]:
                count = 3
                stop = True
            if stop:
                if count > 0:
                    #print splt

                    if 'Stars' in splt[0] and len(splt)>1 :
                        stars = float(splt[1])
                    if 'Author' in splt[0] and len(splt)>1:
                        author = splt[1]
                    if 'Review' in splt[0] and len(splt)>1:
                        cat = classify(stars,splt[1]), ":", splt[1]
                        #if('Good' not in cat):
                        #if('Performance' in cat or 'Energy' in cat):
                        if printflag and 'Good' not in cat[0]:
                            printtofile('Author: '+author)
                            printtofile('Stars: '+str(stars))
                            printtofile('\nComments: '+splt[1])
                            printtofile('Comment classification: '+cat[0]+'\n\n')
                            flag = True
                    count -= 1
            #else:
                #print splt
        if flag :
            link = fname.replace("Crawl01/","https://play.google.com/store/apps/details?id=")
            printtofile('\nLink : '+link)
            printtofile('\n=============================\n')



#get all file in the directory
filenames = next(os.walk(dir_name))[2]
fresult = open('highprofile.txt','w')
for fname in filenames:
    if not fname.endswith(".txt"):
        parse(dir_name+"/"+fname)
fresult.close()
