__author__ = 'Abhijeet'

from htmlutil import scrape_weblink
from fileutil import init_crawl
from fileutil import tape_in
from fileutil import tape_out
from random import randint
from time import sleep
import time,sys

class Crawler:
    crawler_name = ""
    crawl_interval = 2.0
    dir_name = "Crawl01"
    base_url = "https://play.google.com/store/apps/details?id=com.google.android.youtube&hl=en"
    tocrawl = set()
    alreadycrawled = set()
    failedcrawl = set()

    def __init__(self):
        print 'Crawler initialized'
        #print base_url
        init_crawl(Crawler.dir_name,Crawler.base_url)

        #get a set of links that need to be crawled
        Crawler.tocrawl = tape_in(Crawler.dir_name +"/source.txt")

        #get a set of links that have already been crawled
        Crawler.alreadycrawled = tape_in(Crawler.dir_name +"/sink.txt")

        #keep track of failed crawl attempts
        Crawler.failedtocrawl = open(Crawler.dir_name +"/fail.txt",'a+')


    def start_crawl(self,crawler_name):
        self.crawler_name = crawler_name
        print self.crawler_name,' started'
        failedcrawl = 0
        while (len(Crawler.tocrawl)>0) :

            if failedcrawl>3:
                print 'Three consicutive failed crawl attemps, stopping for now'
                Crawler.failedtocrawl.close()
                sys.exit(0)

            start_time = time.time()

            #choose a link to crawl
            nowcrawl = Crawler.tocrawl.pop()

            try:
                #crawl then page
                links = scrape_weblink(nowcrawl,Crawler.dir_name)

                #add this link to the crawled pool
                Crawler.alreadycrawled.add(nowcrawl)

                newlinksadded = False

                #add the new links found from this page into existing to crawl pool, make sure only new links are added
                for l in links:
                    if l not in Crawler.alreadycrawled:
                        newlinksadded = True
                        Crawler.tocrawl.add(l)

                if newlinksadded :
                    print 'new links have been added '

                tape_out(Crawler.tocrawl,Crawler.dir_name +"/source.txt",'w')

                #this is unnecessary work, we only need to write one line at the end of the file
                #tape_out(Crawler.alreadycrawled,Crawler.dir_name +"/sink.txt")
                #this new peice should only append a line
                tape_out({nowcrawl},Crawler.dir_name +"/sink.txt",'a+')


                elapsed_time = time.time() - start_time
                print 'elapsed time ',elapsed_time
                if elapsed_time < Crawler.crawl_interval:
                    waittime = Crawler.crawl_interval - elapsed_time #+ randint(0,1)
                    waittime = abs(waittime)
                    print 'sleeping for '+str(waittime)
                    sleep(waittime)

                failedcrawl = 0

            except IOError:
                print 'error for link '+nowcrawl
                Crawler.failedtocrawl.write(nowcrawl+"\n")
                failedcrawl += 1