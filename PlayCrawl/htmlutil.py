__author__ = 'Abhijeet'

from HTMLParser import HTMLParser
import urllib, re
import sys


url = ""
appname = ""
base_dir = ""

class HParser(HTMLParser):
    def __init__(self):
        HTMLParser.__init__(self)
        self.hlinks = set()
        self.active = -1
        self.data = ""
        self.run = True
        self.overview = True
        self.dump = open(base_dir+"/"+appname,'w')

    def error(self, message):
        pass

    def get_links(self):
        return self.hlinks


    def close_file(self):
        self.dump.close()

    def get_stars(self,starstring):
        star = re.compile('Rated (.*?) stars',re.DOTALL | re.IGNORECASE).findall(starstring)
        return  str(star[0])

    def process_data(self,istag,tag="", attrs=[],star_container=False):
        if istag:
            if self.overview:
                if self.active==2:
                    self.overview = False
                    #print 'review-stats'
                    self.dump.write('\nreview-stats ')
                else:
                    isoverallstar = False
                    for atr in attrs:
                        if isoverallstar:
                            #print 'Overall stars : ',atr[1]
                            self.dump.write('\nOverall stars  ==>data<==  ')
                            self.dump.write(self.get_stars(atr[1]))
                        if 'star-rating-non-editable-container' in atr[1]:
                            isoverallstar = True
            else:
                if self.active==3:
                    #print 'Five stars'
                    self.dump.write('\nFive stars ')
                if self.active==4:
                    #print 'Four stars'
                    self.dump.write('\nFour stars ')
                if self.active==5:
                    #print 'Three stars'
                    self.dump.write('\nThree stars ')
                if self.active==6:
                    #print 'Two stars'
                    self.dump.write('\nTwo stars ')
                if self.active==7:
                    #print 'One stars'
                    self.dump.write('\nOne stars ')

                if self.active==1:
                    #print 'Author'
                    self.dump.write('\nAuthor ')
                if star_container:
                    #print attrs
                    self.dump.write('\nStars ==>data<== ')
                    self.dump.write(self.get_stars(attrs[1][1]))
                    self.dump.write('\nReview ')



        else :
            if not self.overview:
                #print self.data
                self.dump.write("==>data<==")
                self.dump.write(self.data)

    def extract_links(self,attrs):
        for atr in attrs:
            if ('href' in atr[0]):
                if ('/store/apps/details?id=' in atr[1] and appname not in atr[1]):
                    nextcrawl = "https://play.google.com" + atr[1]
                    self.hlinks.add(nextcrawl)

    def handle_starttag(self, tag, attrs):

        if 'a' in tag:
            self.extract_links(attrs)

        if 'div' in tag and self.run:

            if len(attrs) > 0:

                if self.active >= 0:
                    self.process_data(istag=False)
                    #print self.data.replace("  ","")
                    self.data = ""

                self.active = -1


            for atr in attrs:

                if ('recent-change' in atr[1]):
                    self.run = False
                    return

                if('review-text' in atr[1]):
                    self.active = 0
                elif(('author' in atr[1] and 'itemprop' not in atr[0]) or ('review-author-name' in atr[0])) :
                    self.active = 1
                elif('review-info' in atr[1] and 'review-info-star-rating' not in atr[1]):
                    self.active = 1
                elif('reviews-stats' in atr[1]):
                    self.active = 2
                elif('rating-bar-container five' in atr[1]):
                    self.active = 3
                elif('rating-bar-container four' in atr[1]):
                    self.active = 4
                elif('rating-bar-container three' in atr[1]):
                    self.active = 5
                elif('rating-bar-container two' in atr[1]):
                    self.active = 6
                elif('rating-bar-container one' in atr[1]):
                    self.active = 7
                elif('review-body' in atr[1]):
                    self.active = 8

                if self.active >= 0:
                    self.process_data(True,tag,attrs,False)
                    #print tag,attrs

                if 'tiny-star star-rating-non-editable-container' in atr[1]:
                    self.process_data(True,tag,attrs,True)
                    #print tag,attrs

    def handle_data(self, data):
        if self.active >= 0:
            self.data += data
        #print data

    def additional_data(self,content):
        name = re.compile('<title id="main-title">(.*?)</title>', re.DOTALL | re.IGNORECASE).findall(
        content)
        self.dump.write('Name ==>data<== ')
        if len(name) > 0 :
            self.dump.write(name[0])

        genre = re.compile('<span itemprop="genre">(.*?)</span> ', re.DOTALL | re.IGNORECASE).findall(
            content)
        self.dump.write('\nGenre ==>data<== ')
        if len(genre) > 0 :
            self.dump.write(genre[0])


        numDownloads = re.compile('<div class="content" itemprop="numDownloads">(.*?)</div>',
                                  re.DOTALL | re.IGNORECASE).findall(content)
        self.dump.write('\nnumDownloads ==>data<== ')
        if len(numDownloads) > 0 :
            self.dump.write(numDownloads[0])


        softwareVersion = re.compile('<div class="content" itemprop="softwareVersion">(.*?)</div>',
                                     re.DOTALL | re.IGNORECASE).findall(content)
        self.dump.write('\nsoftwareVersion ==>data<== ')
        if len(softwareVersion) > 0:
            self.dump.write(softwareVersion[0])


        operatingSystems = re.compile('<div class="content" itemprop="operatingSystems">(.*?)</div>',
                                      re.DOTALL | re.IGNORECASE).findall(content)
        self.dump.write('\noperatingSystems ==>data<== ')
        if len(operatingSystems) > 0:
            self.dump.write(operatingSystems[0])



def scrape_weblink(weblink,dir_name):
    global url
    global appname
    global base_dir
    base_dir = dir_name
    url = weblink
    print 'getting app webpage ',url
    appname = url.split("=")[1].replace("&hl","")

    page = urllib.urlopen(url)
    content = page.read()


    hp = HParser()
    #print 'getting app metadata'
    try:
        hp.additional_data(content)
    except ValueError:
        print 'error in additional data parsing'
    #print 'getting user comments'
    try:
        hp.feed(content)
    except ValueError:
        print 'error in comment parsing'
    hp.close_file()

    return hp.get_links()



