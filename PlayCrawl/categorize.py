__author__ = 'Abhijeet'
Update = ["update","crash","force close","force stopped"]
Performance = ["slow","hangs","freez","performance","lag"]
Ads = ["ads","advt","advert","popup","pops","adds","Commercial"]
Energy = ["battery","energy","drain","juice"]

def classify(str_rat,str_cmnt):
    type = ""
    rating = int(str_rat)
    if (rating == 5):
        return "Good"

    cat = False

    for u in Update:
        if(u in str_cmnt.lower()):
            type += "Update"
            cat = True
            break
    for p in Performance:
        if(p in str_cmnt.lower()):
            type += "Performance"
            cat = True
            break

    for a in Ads:
        if(a in str_cmnt.lower()):
            type += "Ads"
            cat = True
            break

    for e in Energy:
        if(e in str_cmnt.lower()):
            type += "Energy"
            cat = True
            break

    if(cat == False):
        type += "Other(functionality)"


    return type