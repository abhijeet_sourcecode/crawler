__author__ = 'Abhijeet'
import os,sys
from categorize import classify
import re

dir_name = "Crawl01"
genre = {}
numDown = {}
cmntsperapp = {'0':0,'1-10':0,'11-20':0,'21-30':0,'31-40':0,'41-50':0,'51-60':0,'61-70':0,'71-80':0,'81-90':0,'91-inf':0}
category = {'Good':0,'Update':0,'Performance':0,'Ads':0,'Energy':0,'Other(functionality)':0}
starchart = {'Good':0.0,'Update':0.0,'Performance':0.0,'Ads':0.0,'Energy':0.0,'Other(functionality)':0.0}

catwise = {'Good':[],'Update':[],'Performance':[],'Ads':[],'Energy':[],'Other(functionality)':[]}
issuewise = {'Good':{},'Update':{},'Performance':{},'Ads':{},'Energy':{},'Other(functionality)':{}}

words = 0
comments = 0

allcomments = set()

uninstall = ['uninstall']
causeuninstall = {'Good':0,'Update':0,'Performance':0,'Ads':0,'Energy':0,'Other(functionality)':0}

def getaveragecomment(cmnt):
    global words
    global comments
    try:
        wordList = re.sub("[^\w]", " ",  cmnt).split()
        words += len(wordList)
        comments += 1
        # print 'cmnt ',cmnt
        # print 'com ',comments
        # print 'words ',words
    except:
        print 'getaveragecomment except'


def checkuninstall(type,cmnt):
    for u in uninstall:
        if u in cmnt:
            #print type, ":", cmnt
            causeuninstall[type] += 1


def parse(fname):
    global genre
    stars = 5
    netcomnts = 0
    thisgenre = ""
    author = 'someauthor'
    with open(fname) as fp:
        stop = False
        flag = False

        count = 3
        for line in fp:
            splt = line.split("==>data<==")

            if len(splt) < 2:
               continue


            atr = splt[1].replace("\n","").replace(" ","").replace("&amp;","&")

            if 'Genre' in splt[0]:
                if atr in genre:
                    thisgenre = atr
                    genre[atr] += 1
                else:
                    genre[atr] = 1

            if 'numDownloads' in splt[0]:
                if atr in numDown:
                    numDown[atr] += 1
                else:
                    numDown[atr] = 1

            if 'Author' in splt[0]:
                count = 3
                stop = True
            if stop:
                if count > 0:


                    if 'Stars' in splt[0]:
                        stars = float(splt[1])
                    if 'Author' in splt[0]:
                        author = splt[1]
                    if 'Review' in splt[0]:

                        if splt[1] in allcomments:
                            continue

                        allcomments.add(splt[1])

                        netcomnts += 1
                        #print splt[1]
                        getaveragecomment(splt[1])
                        #cat = classify(stars,splt[1]), ":", splt[1]
                        cat = classify(stars,splt[1])
                        for k in category.keys():
                            if k in cat:
                                category[k] += 1
                                starchart[k] += stars
                                checkuninstall(k,splt[1])
                                if thisgenre in issuewise[k]:
                                    issuewise[k][thisgenre] += 1
                                else:
                                    issuewise[k][thisgenre] = 1

                                # gencat = catwise[k]
                                # gencat.append((thisgenre,stars))


                    count -= 1

    #print 'nbet ',netcomnts
    #bucket netcomnts
    if netcomnts <= 0:
        cmntsperapp['0'] += 1
    elif netcomnts <= 11:
        cmntsperapp['1-10'] += 1
    elif netcomnts <= 21:
        cmntsperapp['11-20'] += 1
    elif netcomnts <= 31:
        cmntsperapp['21-30'] += 1
    elif netcomnts <= 41:
        cmntsperapp['31-40'] += 1
    elif netcomnts <= 51:
        cmntsperapp['41-50'] += 1
    elif netcomnts <= 61:
        cmntsperapp['51-60'] += 1
    elif netcomnts <= 71:
        cmntsperapp['61-70'] += 1
    elif netcomnts <= 81:
        cmntsperapp['71-80'] += 1
    elif netcomnts <= 91:
        cmntsperapp['81-90'] += 1
    else:
        cmntsperapp['91-inf'] += 1


print 'Checking directory ',dir_name
if not os.path.exists(dir_name):
    print 'directory does not exist ',dir_name
    sys.exit(0)

#get all file in the directory
filenames = next(os.walk(dir_name))[2]
print '\nTotal number of files: '+str(len(filenames))
for fname in filenames:
    if not fname.endswith(".txt"):
        parse(dir_name+"/"+fname)

print '\nGenere'
for key in genre:
    print key,",",genre[key]

print '\nDownload stats'
for key in numDown:
    print key,",",numDown[key]

print '\nComments per app'
for key in cmntsperapp:
    print key,",",cmntsperapp[key]

print '\nCategory of comments'
for key in category:
    if category[key] > 0:
        print key,",",category[key],",", str(starchart[key]/category[key])
    else:
         print key,",",category[key],",","-"


print '\n\nComments ',comments,' Words ',words
print 'average ',str(words/comments)

# for key in catwise.keys():
#     print 'Comment classification for key ',key
#     print catwise[key]


print '\nCause uninstall'
for key in causeuninstall:
    print key,",",str((causeuninstall[key]*1000)/category[key])


for issue in issuewise:
    print "=========================="
    print issue
    for key in issuewise[issue]:
        print key, issuewise[issue][key]