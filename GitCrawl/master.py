__author__ = 'Abhijeet'

from HTMLParser import HTMLParser
import urllib, re
import sys,os
from time import sleep
from random import *

url = ""
appname = ""
base_dir = ""

class HParser(HTMLParser):
    def __init__(self):
        HTMLParser.__init__(self)
        self.Flag = False

    def error(self, message):
        pass


    def handle_starttag(self, tag, attrs):
        if 'a' in tag and self.Flag:
            if len(attrs) == 1:
                print attrs
            self.Flag = False

        if 'h3' in tag:
            self.Flag = True




def get_links(url):
    page = urllib.urlopen(url)
    content = page.read()
    hp = HParser()

    try:
        hp.feed(content)
    except ValueError:
        print 'error in comment parsing'


#main function
dir_name = "crawled_data"
if not os.path.exists(dir_name):
    print 'creating a directory for website ',dir_name
    os.makedirs(dir_name)


for page in range(9,12):
    base_url = "https://github.com/search?p="+str(page)+"&q=android&ref=searchresults&type=Repositories"
    #print base_url
    try:
        get_links(base_url)
    except IOError:
        print 'error for link '+base_url

    sleep(10 + randint(0,2))




