__author__ = 'Abhijeet'

from HTMLParser import HTMLParser
import urllib, re
import sys,os
import urllib2
from time import sleep
import time
from random import randint
import requests


class HParser(HTMLParser):
    def __init__(self):
        HTMLParser.__init__(self)
        self.projectname = ""
        self.baseurl = ""
        self.basedir = ""
        self.dumpdir = ""
        self.scrapeID = 0
        self.proxy = ""
        self.skips = 0
        self.crawl_interval = 1
        self.session = requests.Session()

    def error(self, message):
        pass


    def get_issue(self,url,idx):
        proxy_support = urllib2.ProxyHandler({"http":self.proxy})
        opener = urllib2.build_opener(proxy_support)
        urllib2.install_opener(opener)

        try:
            #proxy = {'http': self.proxy}
            #html = urllib.urlopen(url,proxies=proxy).read()
            html = urllib2.urlopen(url).read()
            fname = self.dumpdir + "/issue_" + str(idx) + ".htm"
            #print 'fname ',fname
            #print html
            if "This is not the web page you are looking for" not in html:
                ret = True
                fp = open(fname,"w")
                fp.write(html)
                fp.close()
            else:
                print 'Found a Jedi'
                return False

        except IOError:
            print 'Skipping ',url
            self.skips += 1


        return True


    def get_all_issues(self,id,project,proxy,crawl_interval):
        self.skips = 0
        self.scrapeID = id
        self.proxy = proxy
        self.crawl_interval = crawl_interval
        self.projectname = project
        self.basedir = "scrapper_"+str(self.scrapeID)
        if not os.path.exists(self.basedir):
            print 'creating base directory ',self.basedir
            os.makedirs(self.basedir)

        self.dumpdir = self.basedir + "/" + self.projectname.replace("/","")
        if not os.path.exists(self.dumpdir):
            print 'creating dump directory ',self.dumpdir
            os.makedirs(self.dumpdir)

        self.baseurl = "https://github.com" + self.projectname + "/issues"


        self.session.proxies = {"http": self.proxy}

        Flag = True

        for idx in range(1,1000):
            url = self.baseurl + "/" + str(idx)
            start_time = time.time()
            print 'getting issues for url ',url
            Flag  = self.get_issue(url,idx)
            elapsed_time = time.time() - start_time
            print 'elapsed time ',elapsed_time
            #sys.exit(0)
            if elapsed_time < self.crawl_interval:
                waittime = self.crawl_interval - elapsed_time + randint(0,1)
                waittime = abs(waittime)
                print 'sleeping for '+str(waittime)
                sleep(waittime)

            if not Flag or self.skips >= 3:
                return

