__author__ = 'Abhijeet'
from parse import get_google_comments
from textblob import TextBlob
from nltk.corpus import wordnet
import os

dir_name = "I:\PlayCrawl\Crawl01"
#dir_name = "pc"
result_file = "results_googleplay.htm"

Energy = ["battery","energy","drain","juice"]
black_list = ["just","yet","far","really","here","very","off","close","over","up","hard","thoroughly","fast"]
white_list = ["while","when","constantly"]


def computeintensive(a):
    for w in white_list:
        if w == a.lower():
            return True

def containsTime(wns,a):

    for b in black_list:
        if b == a.lower():
            return False

    for wn in wns:
        definition = wn.definition()
        if "time" in definition:
            return True

    return False


def is_energy(str):
    for e in Energy:
        if e in str:
            return True
    return False


hardware_related = {"screen","volume","gps","wifi","lcd","bluetooth","sensor","cpu","accelerometer","gyroscope","compass","3g","2g","4g","radio","battery"}

def isHardware(cmnt):
    pos = cmnt.split(" ")
    for p in pos:
        for h in hardware_related:
            if h in p.lower():
                #print "found hardware ",h
                return True
    return False

def printinblue(str):
    result_fp.write( """<span style="color:#ff0000">"""+str+"</span> ")

def printinred(str):
    result_fp.write( """<span style="color:#0000ff">"""+str+"</span> ")

def printingreen(str):
    result_fp.write( """<span style="color:#008000">"""+str+"</span> ")

def printnormal(str):
    result_fp.write( str + " ")

def strip_non_ascii(string):
    stripped = (c for c in string if 0 < ord(c) < 127)
    return ''.join(stripped)

def printheaders():
    result_fp.write("<!DOCTYPE html>\n")
    result_fp.write("<html>\n")
    result_fp.write("<body>\n")

    all_energy_fp.write("<!DOCTYPE html>\n")
    all_energy_fp.write("<html>\n")
    all_energy_fp.write("<body>\n")

def printenders():
    result_fp.write( "</html>\n")
    result_fp.write( "</body>\n")

    all_energy_fp.write( "</html>\n")
    all_energy_fp.write( "</body>\n")

def process_comment(c,filename,printheader):
    toreturn = True
    if is_energy(c):
        toreturn = False
        if printheader :
            result_fp.write( "<h2>"+filename+"</h2>\n")


        result_fp.write( "<p>\n")
        tcmnt = TextBlob(c)


        for t in tcmnt.tags:
            flag = False
            (a,b) = t
            if "RB" == b or 'IN' == b:
                wns = wordnet.synsets(a)
                if containsTime(wns,a):
                    printinblue(a)
                    flag = True

            if computeintensive(a):
                printinblue(a)
                flag = True

            if isHardware(a) and not flag:
                printinred(a)
                flag = True

            if not flag:
                printnormal(a)

        result_fp.write( "</p>\n")

    return toreturn


def can_print(c):
    retflag = False
    if is_energy(c):
        tcmnt = TextBlob(c)
        for t in tcmnt.tags:
            flag = False
            (a,b) = t
            if "RB" == b or 'IN' == b:
                wns = wordnet.synsets(a)
                if containsTime(wns,a) or computeintensive(a):
                    retflag = True
                    break

        if not retflag:
            return False

        for t in tcmnt.tags:
            flag = False
            (a,b) = t

            for h in hardware_related:
                if h in a:
                    return True

    return False

total_comments = 0
final_comments = 0
result_fp = open(result_file,"w")
all_energy_fp = open("all_energy.htm","w")
printheaders()
filenames = next(os.walk(dir_name))[2]
for fname in filenames:
    if not fname.endswith(".txt"):
        print "Processing file ",fname
        acmnt = get_google_comments(dir_name+"/"+fname)
        if len(acmnt) > 0:
            all_energy_fp.write( "<h2>"+fname+"</h2>\n")
        #print acmnt
        total_comments += len(acmnt)
        ret = True
        for c in acmnt:
            c = strip_non_ascii(c)

            all_energy_fp.write("<p>\n")
            all_energy_fp.write(c)
            all_energy_fp.write("</p>\n")

            if can_print(c):
                final_comments += 1
                ret = process_comment(c,fname,ret) and ret


result_fp.write( "<H1>\n")
top = "total number of user comments "+str(total_comments)
result_fp.write(top)
result_fp.write( "\n")
result_fp.write( "</H1>\n")

result_fp.write( "<H1>\n")
top = "number of user comments  with energy keywords, temporal keywords and hardware keywords "+str(final_comments)
result_fp.write(top)
result_fp.write( "\n")
result_fp.write( "</H1>\n")

printenders()
result_fp.close()




