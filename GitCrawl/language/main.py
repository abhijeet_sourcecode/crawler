__author__ = 'Abhijeet'
from parse import get_comments
from textblob import TextBlob
from nltk.corpus import wordnet
from nltk.stem.wordnet import WordNetLemmatizer

import os

dir_name = "eissue3"
result_file = "results_eissue3_temporal_withuserevents.htm"

Energy = ["battery","energy","drain","juice"]
black_list = ["just","yet","far","really","here","very","off","close","over","up","hard","thoroughly","fast","only","sure","long"]
white_list = ["while","when","constantly","forever","until"]


def computeintensive(a):
    for w in white_list:
        if w == a.lower():
            return True

def containsTime(wns,a):

    for b in black_list:
        if b == a.lower():
            return False

    for wn in wns:
        definition = wn.definition()
        if "time" in definition:
            return True

    return False


def is_energy(str):
    for e in Energy:
        if e in str:
            return True
    return False


hardware_related = {"screen","volume","gps","wifi","lcd","bluetooth","sensor","cpu","accelerometer","gyroscope","compass","3g","2g","4g","radio","battery","energy","wakelock"}

def isHardware(cmnt):
    pos = cmnt.split(" ")
    for p in pos:
        for h in hardware_related:
            if h in p.lower():
                #print "found hardware ",h
                return True
    return False

allverbs = {'open','start','launch','resume','close','exit','kill','stop','pause','run','instal','uninstall','click','tap','drag','move','turn','run','rotate','shake','choose','select','disconnect','connect','force-stop','walk','sit','edit'}

def userevent(a,tag):



    if "VB" in tag:
        for v in allverbs:
            if v in a.lower():
                return True
    else:
        return False


def printinblue(str):
    result_fp.write( """<span style="color:#ff0000">"""+str+"</span> ")

def printinred(str):
    result_fp.write( """<span style="color:#0000ff">"""+str+"</span> ")

def printingreen(str):
    result_fp.write( """<span style="color:#008000">"""+str+"</span> ")

def printnormal(str):
    result_fp.write( str + " ")

def strip_non_ascii(string):
    stripped = (c for c in string if 0 < ord(c) < 127)
    return ''.join(stripped)

def printheaders():
    result_fp.write("<!DOCTYPE html>\n")
    result_fp.write("<html>\n")
    result_fp.write("<body>\n")

def printenders():
    result_fp.write( "</html>\n")
    result_fp.write( "</body>\n")


def process_comment(c,filename,printheader):
    toreturn = True
    if is_energy(c):
        toreturn = False
        if printheader :
            result_fp.write( "<h2>"+filename+"</h2>\n")


        result_fp.write( "<p>\n")
        tcmnt = TextBlob(c)

        print c
        print tcmnt.tags

        for t in tcmnt.tags:
            flag = False
            (a,b) = t
            if "RB" in b or 'IN' in b or  ('JJ' in b and "-" in a):
                wns = wordnet.synsets(a)
                if containsTime(wns,a):
                    printinblue(a)
                    flag = True

            if computeintensive(a):
                printinblue(a)
                flag = True

            if userevent(a,b):
                printingreen(a)
                flag = True


            if isHardware(a) and not flag:
                printinred(a)
                flag = True

            if not flag:
                printnormal(a)

        result_fp.write( "</p>\n")

    return toreturn


def can_print(c):
    if is_energy(c):
        tcmnt = TextBlob(c)
        for t in tcmnt.tags:
            flag = False
            (a,b) = t
            if "RB" in b or 'IN' in b or ('JJ' in b and "-" in a):
                wns = wordnet.synsets(a)
                if containsTime(wns,a) or computeintensive(a):
                    return True
    return False

total_comments = 0
final_comments = 0
result_fp = open(result_file,"w")
printheaders()
filenames = next(os.walk(dir_name))[2]
for fname in filenames:
    if fname.endswith(".htm"):
        print "Processing file ",fname
        acmnt = get_comments(dir_name+"/"+fname)
        total_comments += len(acmnt)
        ret = True
        for c in acmnt:
            c = strip_non_ascii(c)
            if can_print(c):
                final_comments += 1
                ret = process_comment(c,fname,ret) and ret


result_fp.write( "<H1>\n")
top = "number of user comments in the issue threads with energy keywords "+str(total_comments)
result_fp.write(top)
result_fp.write( "\n")
result_fp.write( "</H1>\n")

result_fp.write( "<H1>\n")
top = "number of user comments in the issue threads with energy keywords, temporal keywords and hardware keywords "+str(final_comments)
result_fp.write(top)
result_fp.write( "\n")
result_fp.write( "</H1>\n")

printenders()
result_fp.close()




