__author__ = 'Abhijeet'
from parse import get_comments
from textblob import TextBlob
from nltk.corpus import wordnet
from nltk.stem.wordnet import WordNetLemmatizer

import os

result_file = "highlighter.htm"

Energy = ["battery","energy","drain","juice"]
black_list = ["just","yet","far","really","here","very","off","close","over","up","hard","thoroughly","fast","only","sure","long"]
white_list = ["while","when","constantly","forever","until"]


def computeintensive(a):
    for w in white_list:
        if w == a.lower():
            return True

def containsTime(wns,a):

    for b in black_list:
        if b == a.lower():
            return False

    for wn in wns:
        definition = wn.definition()
        #print a,':',definition
        if "time" in definition:
            return True

    # if "-time" in a:
    #     return True

    return False


def is_energy(str):
    for e in Energy:
        if e in str:
            return True
    return False


hardware_related = {"screen","volume","gps","wifi","lcd","bluetooth","sensor","cpu","accelerometer","gyroscope","compass","3g","2g","4g","radio","battery","energy","wakelock","location"}

def isHardware(cmnt):
    pos = cmnt.split(" ")
    for p in pos:
        for h in hardware_related:
            if h in p.lower():
                #print "found hardware ",h
                return True
    return False

allverbs = {'open','start','launch','resume','close','exit','kill','stop','pause','run','instal','uninstall','click','tap','drag','move','turn','run','rotate','shake','choose','select','disconnect','connect','force-stop','walk','sit','edit'}

def userevent(a,tag):



    if "VB" in tag:
        for v in allverbs:
            if v in a.lower():
                return True
    else:
        return False


def printinblue(str):
    result_fp.write( """<span style="color:#0000ff">"""+str+"</span> ")
    print str,',hardware'


def printinred(str):
    result_fp.write( """<span style="color:#ff0000">"""+str+"</span> ")
    print str,',temporal'

def printingreen(str):
    result_fp.write( """<span style="color:#008000">"""+str+"</span> ")
    print str,',event'

def printnormal(str):
    result_fp.write( str + " ")
    print str,',none'

def strip_non_ascii(string):
    stripped = (c for c in string if 0 < ord(c) < 127)
    return ''.join(stripped)

def printheaders():
    result_fp.write("<!DOCTYPE html>\n")
    result_fp.write("<html>\n")
    result_fp.write("<body>\n")

def printenders():
    result_fp.write( "</html>\n")
    result_fp.write( "</body>\n")


def process_comment(c,filename,printheader):
    print c
    toreturn = True
    if is_energy(c):
        toreturn = False
        if printheader :
            result_fp.write( "<h2>"+filename+"</h2>\n")


        result_fp.write( "<p>\n")
        tcmnt = TextBlob(c)

        #print c
        #print tcmnt.tags

        for t in tcmnt.tags:
            flag = False
            (a,b) = t
            if "RB" in b or 'IN' in b or  ('JJ' in b and "-" in a):

                tosend = a
                if "-" in a:
                    tosend = a.split("-")[-1]
                else:
                    tosend = a
                #print 'processing ',a, " to send ",tosend
                wns = wordnet.synsets(tosend)
                #print wns
                if containsTime(wns,tosend):
                    printinred(a)
                    flag = True

            if computeintensive(a) and not flag:
                printinred(a)
                flag = True

            if userevent(a,b):
                printingreen(a)
                flag = True


            if isHardware(a) and not flag:
                printinblue(a)
                flag = True

            if not flag:
                printnormal(a)

        result_fp.write( "</p>\n")

    return toreturn


def can_print(c):
    if is_energy(c):
        tcmnt = TextBlob(c)
        #print tcmnt.tags
        for t in tcmnt.tags:
            flag = False

            (a,b) = t
            if "RB" in b or 'IN' in b or ('JJ' in b and "-" in a):
                #print a
                wns = wordnet.synsets(a)
                if containsTime(wns,a) or computeintensive(a):
                    return True
    return False


tohgh = {"When I open Bitcoin-Android the circular thinking/downloading thing in the top right corner keeps on spinning forever When I close that window the app is still draining my CPU at over 90 %",
         "When debugging some issues I noticed that setExactPos was being called constantly while in a loop while sitting on the expanded activity. This must be one of the things that is causing lag and killing battery life",
         "Not sure if this is possible but maybe we can improve positioning to be battery friendly When running around with GPS turned on my phone is dead within a few hours whereas it normally lasts all day long",
         "Every-time the list is updated, the Calendar app shows nothing. It is empty. It is that so for a few-minutes until it regenerates all its data. A bad side effect is the battery drain. If you do edit tasks too often with calendar reminders activated, once you have done take a look at the battery level and go check the battery statistics. Calendar provider will be on top of it.",
         "Motion does not ask for location updates until the user begins recording data this is good However once done recording my Nexus 5 indicates it is still collecting location data It should probably unregister this location listener when the user stops and re-enable it when the user starts again to conserve battery",
         "I think that it is a very important issue because there is a lot of battery usage reported to the application because service is never stopped",
         "For example if you are trying to detect when the user is walking outside his house then you should fetch his speed every-minute forever which will drain much battery",
         "In order to reduce battery usage all protocols providers should be disabled after some user inactivity The app woken up only when there is incoming call or chat is started",
         "This might be new with 4.4.2 but OpenXC Enabler is indirectly causing a HUGE number of wakelocks and massive battery drain on my phone when running Whenever it attempts to start the serial connection to the VI which is constantly android uses a wakelock to wake up the CPU to perform the check This is causing a near 100 % awake-time on my device I have not tested other devices yet As soon as I force-stop the enabler the wakelocks go away tested using theWakeLock Detector app running an Xposed module to fix it for 4.4",
         "The 1.5.0 is still draining my battery 40GB of data I restarted a refresh after killing the initial refresh when I noticed my phone was hot and 75 % battery usage was Owncloud App This is how I came across this issue The refresh was started from the main index not within a folder",
         }







result_fp = open(result_file,"w")
printheaders()
count = 0
for t in tohgh:
    ret = False
    t = strip_non_ascii(t)
    if can_print(t):
        count =+ 1
        ret = process_comment(t,str(count),ret) and ret


printenders()
result_fp.close()




